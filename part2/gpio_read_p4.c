#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>

int main()
{
    // For writing output gpio
    int fd1;

    //Enable gpio27
    int fd = open("/sys/class/gpio/export", O_WRONLY);
    write(fd, "27", 2);
    write(fd, "17", 2);
    close(fd);

    //Set gpio27 as input
    fd = open("/sys/class/gpio/gpio27/direction", O_WRONLY);
    fd1 = open("/sys/class/gpio/gpio17/direction", O_WRONLY);
    write(fd, "in", 2);
    write(fd1, "out", 3);
    close(fd);
    close(fd1);

    //Set gpio27 interrupt
    fd = open("/sys/class/gpio/gpio27/edge", O_WRONLY);
    // write(fd, "falling", 7);
    write(fd, "both", 4);
    close(fd);


    // struct pollfd pfd;
    // pfd.fd = fd;
    // pfd.events = POLLPRI;

    for(int i=0; i<100000; i++)
    {
        //Wait for event
        fd = open("/sys/class/gpio/gpio27/value", O_RDONLY);
        fd1 = open("/sys/class/gpio/gpio17/value", O_WRONLY);      
        // int ret = poll(&pfd, 1, 3000);
        char c;
        read(fd, &c, 1);
        close(fd);
        // if(ret == 0)
            // printf("Timeout\n");
        // else
        if(c == '0')
        {
            printf("Push\n");
            write(fd1, "00", 2);
        }
                
        else
        {
            printf("Release\n");
            write(fd1, "01", 2);
        }
        close(fd1);   
    }

    //Disable gpio27
    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    write(fd, "27", 2);
    write(fd, "17", 2);
    close(fd);

    return(0);
}
