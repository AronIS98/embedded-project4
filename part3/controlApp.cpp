#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <stdlib.h>

int fd;
float pulses = 0;
// https://pubs.opengroup.org/onlinepubs/000095399/functions/read.html
char c[20];
size_t nbytes;
ssize_t bytes_read;
float et = 0;
float curr_pwm;
float max_speed = 1300;

float scaler = 100000;
float error;
int update(float ref, float actual){
    // printf("Value of actual: %f\n", actual);
    
    printf("Value of reference speed (PPS): %f\n", ref);
    printf("Value of actual speed (PPS): %f", actual*10);
    float test1 = ref - actual*10;
    float converter = (test1/max_speed)*scaler;

    float kp =  0.0011;
    float ti = 3.2;
    float t = 0.9;
    
    
    float add = et + (test1*t); // cumulative sum

    et = add;


    float i = (1/ti) * et; 
    curr_pwm =  (kp*(test1 + i))*scaler;
    if(curr_pwm > scaler-1){curr_pwm = scaler-1;}
    if(curr_pwm <1){curr_pwm = 0;}

    return (int)curr_pwm;
}



void set_pwm(int update_value)
{

    fd = open("/sys/class/pwm/pwmchip0/pwm0/duty_cycle", O_WRONLY);
    char buffer[5];
    sprintf(buffer, "%d", update_value);
    printf(buffer);
    printf("\n");
    write(fd, buffer, 5);
    close(fd);
}



int count_pulses()
{

    fd = open("/sys/module/encoder_counter/parameters/numberPresses", O_RDONLY);       
    nbytes = sizeof(c);
    bytes_read = read(fd, &c, nbytes);
    c[bytes_read] = '\0';
    pulses = atoi(c);

    close(fd);
    return pulses;

}

void init_pwm()
{
    // Initialized to 50 duty cycle% 
    fd = open("/sys/class/pwm/pwmchip0/export", O_WRONLY);
    write(fd, "00", 2);
    close(fd);

    fd = open("/sys/class/pwm/pwmchip0/pwm0/period", O_WRONLY);
    write(fd, "100000", 6);
    close(fd);

    fd = open("/sys/class/pwm/pwmchip0/pwm0/duty_cycle", O_WRONLY);
    write(fd, "50000", 5);
    close(fd);

    fd = open("/sys/class/pwm/pwmchip0/pwm0/enable", O_WRONLY);
    write(fd, "01", 2);
    close(fd);

}

void setup_gpio()
{
    int fd = open("/sys/class/gpio/export", O_WRONLY);
    write(fd, "27", 2);
    write(fd, "17", 2);
    write(fd, "22", 2);
    close(fd);

    fd = open("/sys/class/gpio/gpio27/direction", O_WRONLY);
    write(fd, "in", 2);
    close(fd);

    fd = open("/sys/class/gpio/gpio17/direction", O_WRONLY);
    write(fd, "out", 3);
    close(fd);

    fd = open("/sys/class/gpio/gpio22/direction", O_WRONLY);
    write(fd, "out", 3);
    close(fd);

    fd = open("/sys/class/gpio/gpio17/value", O_WRONLY);   
    write(fd, "01", 2);
    close(fd);

    fd = open("/sys/class/gpio/gpio22/value", O_WRONLY);   
    write(fd, "00", 2);
    close(fd);

}

void exit_pwm()
{
    fd = open("/sys/class/pwm/pwmchip0/unexport", O_WRONLY);
    write(fd, "00", 2);
    close(fd);
}


void gpio_cleanup()
{
    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    write(fd, "27", 2);
    write(fd, "17", 2);
    write(fd, "22", 2); 
    close(fd);
}

int main()
{


    int i = 0;


    init_pwm();
    setup_gpio();
    int old_val = 0;
    int new_pwm = 0;
    int speed = 0;
    int ref_speed = 700;
    int modifier = 0;
    while(i<10000) //Runs for a predefined period of time
    {

        pulses = count_pulses();
        if (i == 0)
        {
            modifier = pulses;
        }
        speed = ((pulses-modifier) - old_val);
        new_pwm = update(ref_speed, speed);
        set_pwm(new_pwm);
        old_val = pulses-modifier;

        usleep(100000); // Update rate

        i++;
        
    }
    exit_pwm();
    gpio_cleanup();

    return(0);
}